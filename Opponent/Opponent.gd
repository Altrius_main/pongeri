extends KinematicBody2D

var Speed = 400 # The movement speed of the opponent
var Ball # Reference to the Ball, used for the basic ai functionality
var DualGame = false # true lets another player to join

# Called when game starts
func _ready():
	Ball = get_parent().find_node("Ball") # Finds the reference to the ball from among parent's nodes

# Physics calculation step
func _physics_process(delta):
	if DualGame: # If DualGame is enabled, calls player_movement to handle movement via keyboard input
		player_movement()
		if Input.is_key_pressed(KEY_T): DualGame = false # Disables the dualgame with T
	else: # If DualGame is not enabled, uses the basic ai to move
		move_and_slide(Vector2(0, get_opponent_direction()) * Speed) # Moves only in y-axis to direction given by get_opponent_direction() -function
		if Input.is_key_pressed(KEY_R): DualGame = true # Enables the dualgame with R
	
func player_movement():
	var Velocity = Vector2.ZERO# Creates a variable for movement direction
	if Input.is_action_pressed("ui_up"):
		Velocity.y -= 1 # If w is pressed, the direction is up
	if Input.is_action_pressed("ui_down"):
		Velocity.y += 1 # If s is pressed, the direction is down
	move_and_slide(Velocity * Speed) # Finally move_and_slide -function handles the movement with it's input


# Basic ai implementation for the opponent. Returns the direction which to move towards to
func get_opponent_direction():
	if Ball.Velocity.x < 0: # If the ball is moving away, guides the opponent towards the middle of it's area
		if position.y < 350: return 1
		elif position.y > 370: return -1
		else: return 0
	elif abs(Ball.position.y - position.y) > 25: # If the ball is far enoug from the middle point of the opponent, moves towards it
		if Ball.position.y > position.y: return 1
		else: return -1
	else: return 0	# If nothing triggered any movement, return 0 to stay at position
