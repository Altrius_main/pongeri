extends KinematicBody2D

var Speed = 400 # Player's speed

# Physics step. Moves the player up and down according to input
func _physics_process(delta):
	var Velocity = Vector2.ZERO # Creates a variable for movement direction
	if Input.is_key_pressed(KEY_W):
		Velocity.y -= 1 # If w is pressed, the direction is up
	if Input.is_key_pressed(KEY_S):
		Velocity.y += 1 # If s is pressed, the direction is down
	move_and_slide(Velocity * Speed) # Finally move_and_slide -function handles the movement with it's input
