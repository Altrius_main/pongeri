extends KinematicBody2D

# Set variables for the ball
const InitialSpeed = 600 # The base speed
var Speed = InitialSpeed # The current speed
var Velocity = Vector2.ZERO # The velocity vector
var Hitsound # Audiostreamplayer for hit sound
var Timer # Countdown timer for launching the ball
var ScytheUp # Scythe sprites used for countdown animation
var ScytheDown

# Automaticly called at start
# I'm not sure how efficient $ -operations are so I'm saving the references just in case for future use
func _ready():
	Hitsound = $Hitsound
	Timer = $Timer
	ScytheUp = $Ball/ScytheU
	ScytheDown = $Ball/ScytheD
	randomize() # randomizes the randomization seed for randomize_Velocity -function
	initialize_ball()

# This is called at each physics calculation step
func _physics_process(delta):
	var collision_object = move_and_collide(Velocity * Speed * delta) # move_and_collide moves the ball with the input
	if collision_object: # If move_and_collide -movement collides, the movement stops and collision object is returned
		Hitsound.play() # Calls audiostreamplayer to play the hit sound
		Velocity = Velocity.bounce(collision_object.normal) # Sets collision normal as new direction to move towards to
	ScytheUp.position.y = Timer.time_left * -1000 # Handles the countdown scythe position relative to the ball itself
	ScytheDown.position.y = Timer.time_left * 1000 # Any time the countdown is 0, the scythes are under the ball

# Initializes the ball position and starts the countdown for launch
func initialize_ball():
	position = Vector2(640, 360)
	Speed = 0
	Timer.start() # Start's the countdown, after which _on_Timer_timeout is called to launch the ball

# Sets the ball's velocity direction randomly from set amount of possibilities
func randomize_Velocity():
	Velocity.x = [-1,1][randi() % 2]
	Velocity.y = [-0.85, -0.75, 0.85, 0.75][randi() % 4]

# Launch the ball when the launchtimer hits 0 
func _on_Timer_timeout():
	randomize_Velocity() # Calls function to randomize the ball's starting direction
	Speed = InitialSpeed
