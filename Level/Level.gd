extends Node

var PlayerScore = 0
var OpponentScore = 0
var Ball
var PointSound

func _ready():
	Ball = $Ball
	PointSound = $Pointsound

func _on_LeftArea_body_entered(body):
	Ball.initialize_ball()
	PointSound.play()
	OpponentScore += 1

func _on_RightArea_body_entered(body):
	Ball.initialize_ball()
	PointSound.play()
	PlayerScore += 1

func _process(delta):
	if $Player.position.x != 30: $Player.position.x = 30
	if $Opponent.position.x != 1250: $Opponent.position.x = 1250
	$PlayerScore.text = str(PlayerScore)
	$OpponentScore.text = str(OpponentScore)
